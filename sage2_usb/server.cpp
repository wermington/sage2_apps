#include "libusb.h"

#include <string>
#include <iostream>
#include <sstream>
#include <stdlib.h>


int main(int argc, char ** argv)
{
	std::string configFile = "usb.cfg";
	int count = 1;

	while(count < argc)
	{
		std::string s = argv[count];

		if(s == "-f")
		{
			count++;
			if(count < argc)
			{
				configFile = argv[count];
			}
		}
		else if(s == "-h" || s == "--help")
		{
			std::cerr << "Usage: " << argv[0] << " [-f configFile]" << std::endl;
			return 0;
		}

		count++;
	}

	std::cerr << "Starting server:" << std::endl;
	std::cerr << "Config File: " << configFile << std::endl;
	std::cerr << "Device Name: Device0" << std::endl;

	libusb * lusb = new libusb("Device0",configFile);

	while(!lusb->isError())
	{
		lusb->mainloop();
	}

	delete lusb;
	//delete connection;

	return 1;
}
