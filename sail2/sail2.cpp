// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014


#include "sail2.h"
#include <time.h>


std::vector <std::string> split(std::string, char);

void ws_open(WebSocketIO *);

void ws_initialize(WebSocketIO *, boost::property_tree::ptree);

void ws_requestNextFrame(WebSocketIO *, boost::property_tree::ptree);

void ws_stopMediaCapture(WebSocketIO *, boost::property_tree::ptree);

void ws_setItemPositionAndSize(WebSocketIO *, boost::property_tree::ptree);

void ws_finishedResize(WebSocketIO *, boost::property_tree::ptree);

void ws_setupDisplayConfiguration(WebSocketIO *, boost::property_tree::ptree);


bool continuous_resize = true;
clock_t this_time;
clock_t last_time;

static int sock_send = 0;

sail::sail(const char *appname, int ww, int hh, enum sagePixFmt pixelfmt, std::string hostname, float frate) :
	width(ww),
	height(hh),
	format(pixelfmt),
	frameRate(frate),
	applicationName(appname),
	ws_host(hostname),
	got_request(false),
	got_frame(false)
{
	applicationName = std::string(appname);

	// Create the websocket for SAGE2
	std::string ws_uri = "ws://" + std::string(hostname);
	fprintf(stderr, "URI to SAGE2: %s\n", ws_uri.c_str());
	wsio = new WebSocketIO(ws_uri, this);
	wsio->open(ws_open);
}

sail::~sail()
{
	// Close the socket
	//wsio->close();

	frame.free_buff();
}

int sail::getBufferSize()
{
	if (format == PIXFMT_888)
		return (width * height * 3);
	else if (format == PIXFMT_YUV)
		return (width * height * 2);
	else return 0;
}

static int _initialized = 0;

sail *createSAIL(const char *appname, int ww, int hh,
                 enum sagePixFmt pixelfmt, const char *url, float frate)
{
	sail *sageInf;

	// Initialize a few things
	if (!_initialized) {
		initTime();
		_initialized = 1;
	}

	// Allocate the sail object
	sageInf = new sail(appname, ww, hh, pixelfmt, url, frate);
	if (sageInf == NULL)
		return NULL;

	return sageInf;
}


void deleteSAIL(sail *sageInf)
{
	delete sageInf;
	exit(0);
}


struct sail_buffer sail::nextBuffer()
{

	struct sail_buffer frame = getBuffer();

	struct sail_buffer out_frame = frame;


	if (frame.comp) {
		out_frame = convertToJpeg(frame.data, getWidth(), getHeight());
		out_frame.id = frame.id;
	}

	size_t len;
	struct sail_buffer base64;
	base64.data = (unsigned char *) base64_encode(out_frame.data, out_frame.size, &(base64.size));
	base64.dealoc = true;
	base64.id = out_frame.id;

	if (frame.comp) {
		out_frame.free_buff();
	}

	return base64;
}

void sail::swapWithBufferCopy(struct sail_buffer frame)
{
	// Swap the main sail object

	struct sail_buffer cpy_data;
	cpy_data.data = (unsigned char *) malloc(sizeof(unsigned char) * frame.size);
	cpy_data.size = frame.size;
	memcpy(cpy_data.data, frame.data, sizeof(unsigned char) * frame.size);
	cpy_data.dealoc = true;


	setBuffer(cpy_data);
	swapBuffer();
}

void sail::swapWithBuffer(struct sail_buffer frame)
{
	// Swap the main sail object
	setBuffer(frame);
	swapBuffer();
}

void sail::swapBuffer()
{
	if (!got_request) return;
	got_request = true;
	got_frame = false;

	sock_send++;

	this_time = clock();

	if (this_time - last_time > CLOCKS_PER_SEC * 5) {
		last_time = this_time;
		printf("\n[SAIL2] Packets send per 5 sec: %d (1 sec: [%.4f] ) \n", sock_send, (float) sock_send / 5);
		sock_send = 0;
	}

	boost::property_tree::ptree state;

	struct sail_buffer buf = nextBuffer();
	if (buf.data == NULL) return;
	state.put<std::string>("src", (char *) buf.data);
	state.put<std::string>("type", "image/jpeg");
	state.put<std::string>("encoding", "base64");

	boost::property_tree::ptree emit_data;
	emit_data.put<std::string>("id", uniqueID + "|0");
	emit_data.put_child("state", state);
	wsio->emit("updateMediaStreamFrame", emit_data);
	buf.free_buff();

}


void sail::processMessages()
{
	// Is the socket closed
	if (wsio->isDone()) {
		exit(0);
	}

	// Not sure if needed
	wsio->runOne();
}


/******** Helper Functions ********/
std::vector <std::string> split(std::string s, char delim)
{
	std::stringstream ss(s);
	std::string item;
	std::vector <std::string> elems;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
	return elems;
}

/******** WebSocket Callback Functions ********/
void ws_open(WebSocketIO *ws)
{
	sail *sageInf = (sail *) ws->getPointer();
	fprintf(stderr, "WEBSOCKET OPEN %s\n", sageInf->applicationName.c_str());

	ws->on("initialize", ws_initialize);
	ws->on("requestNextFrame", ws_requestNextFrame);
	ws->on("stopMediaCapture", ws_stopMediaCapture);
	ws->on("setupDisplayConfiguration", ws_setupDisplayConfiguration);

	boost::property_tree::ptree data;
	data.put<std::string>("clientType", sageInf->applicationName);
	boost::property_tree::ptree req;
	req.put<bool>("config", true);
	req.put<bool>("version", false);
	req.put<bool>("time", false);
	req.put<bool>("console", false);
	data.put_child("requests", req);

	ws->emit("addClient", data);
}

// {
//     name: "mysite",
//     host: "localhost",
//     port: 9090,
//     index_port: 9292,
//     resolution: {
//         width: 1920,
//         height: 1080
//     },
//     layout: {
//         rows: 1,
//         columns: 1
//     },
//     displays: [
//         {
//             row: 0,
//             column: 0
//         }
//     ],
//     totalWidth: 1920,
//     totalHeight: 1080,
// }

void ws_setupDisplayConfiguration(WebSocketIO *, boost::property_tree::ptree data)
{
	std::string dname = data.get<std::string>("name");
	int dwidth = data.get<int>("resolution.width");
	int dheight = data.get<int>("resolution.height");
	fprintf(stderr, "Display> %s %d %d\n", dname.c_str(), dwidth, dheight);
}

void ws_initialize(WebSocketIO *ws, boost::property_tree::ptree data)
{
	sail *sageInf = (sail *) ws->getPointer();
	sageInf->uniqueID = data.get<std::string>("UID");
	fprintf(stderr, "SAGE2 ID: %s\n", sageInf->uniqueID.c_str());

	if (!sageInf->uniqueID.empty()) {
		fprintf(stderr, "Initializing connection\n");

		boost::property_tree::ptree emit_data;
		emit_data.put<std::string>("id", sageInf->uniqueID + "|0");
		emit_data.put<std::string>("title", sageInf->applicationName);
		// black pixel 1x1 GIF
		emit_data.put<std::string>("src", "R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==");
		emit_data.put<std::string>("type", "image/gif");
		emit_data.put<std::string>("encoding", "base64");
		emit_data.put<int>("width", sageInf->getWidth());
		emit_data.put<int>("height", sageInf->getHeight());
		ws->emit("startNewMediaStream", emit_data);
	}
}

void ws_requestNextFrame(WebSocketIO *ws, boost::property_tree::ptree data)
{
	sail *sageInf = (sail *) ws->getPointer();

	sageInf->got_request = true;
	if (sageInf->got_frame) {
		sageInf->swapBuffer();
	}
}

void ws_stopMediaCapture(WebSocketIO *ws, boost::property_tree::ptree data)
{
	std::string streamId = data.get<std::string>("streamId");
	int idx = atoi(streamId.c_str());
	fprintf(stderr, "STOP MEDIA CAPTURE: %d\n", idx);

	sail *sageInf = (sail *) ws->getPointer();
	deleteSAIL(sageInf);
}

void ws_setItemPositionAndSize(WebSocketIO *ws, boost::property_tree::ptree data)
{
	sail *sageInf = (sail *) ws->getPointer();

	// update browser window size during resize
	if (continuous_resize) {
		std::string id = data.get<std::string>("elemId");
		std::vector <std::string> elemData = split(id, '|');
		std::string uid = "";
		int idx = -1;
		if (elemData.size() == 2) {
			uid = elemData[0];
			idx = atoi(elemData[1].c_str());
		}

		if (uid == sageInf->uniqueID) {
			std::string w = data.get<std::string>("elemWidth");
			std::string h = data.get<std::string>("elemHeight");
			std::string x = data.get<std::string>("elemLeft");
			std::string y = data.get<std::string>("elemTop");
			float neww = atof(w.c_str());
			float newh = atof(h.c_str());
			float newx = atof(x.c_str());
			float newy = atof(y.c_str());
			printf("New pos & size: %.0f x %.0f - %.0f x %.0f\n", newx, newy, neww, newh);
		}
	}
}

void ws_finishedResize(WebSocketIO *ws, boost::property_tree::ptree data)
{
	sail *sageInf = (sail *) ws->getPointer();

	std::string id = data.get<std::string>("id");
	std::vector <std::string> elemData = split(id, '|');
	std::string uid = "";
	int idx = -1;
	if (elemData.size() == 2) {
		uid = elemData[0];
		idx = atoi(elemData[1].c_str());
	}

	if (uid == sageInf->uniqueID) {
		std::string w = data.get<std::string>("elemWidth");
		std::string h = data.get<std::string>("elemHeight");

		float neww = atof(w.c_str());
		float newh = atof(h.c_str());

		fprintf(stderr, "New size: %.0f x %.0f\n", neww, newh);
	}
}

