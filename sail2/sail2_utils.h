// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014


#include <stdlib.h>
#include <stdint.h>
#include <fcntl.h>
#include <math.h>
#include <ctime>
#include <stdio.h>
#include <string>
#include <string.h>

#ifndef __SAIL2_UTILS__
#define __SAIL2_UTILS__


struct sail_buffer
{
	unsigned char *data;
	size_t size;
	bool comp;
	bool dealoc;
	size_t id;

	sail_buffer() : data(NULL), size(0), comp(false), dealoc(false) {}

	void free_buff()
	{
		if(dealoc)
		{
			free(data);
			data = NULL;
			size = 0;
			dealoc = false;
		}
	}

};


struct sail_buffer sail_createBuffer(unsigned char * buffer, size_t size, bool comp);

char *base64_encode(const unsigned char *data, std::size_t input_length, size_t *output_length);

struct sail_buffer convertToJpeg(unsigned char* buffer, int width, int height);

double getTime();
void   initTime();

#endif
