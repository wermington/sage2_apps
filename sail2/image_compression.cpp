// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014

#include "sail2_utils.h"

#include <jpeglib.h>

void BGRAtoRGB(const unsigned char* bgra, int pixel_width, unsigned char* rgb);
void YUV422toRGB(const unsigned char* yuv, int pixel_width, int pixel_height, unsigned char* rgb);
int clamp(int n, int lower, int upper);


struct sail_buffer convertToJpeg(unsigned char* rgb, int width, int height)
{
	jpeg_compress_struct cinfo;
	jpeg_error_mgr jerr;

	cinfo.err = jpeg_std_error(&jerr);
	jerr.trace_level = 0;

	jpeg_create_compress(&cinfo);
	uint8_t *jpeg_buffer_raw = NULL;
	unsigned long outbuffer_size = 0;
	jpeg_mem_dest(&cinfo, &jpeg_buffer_raw, &outbuffer_size);
	cinfo.image_width = width;
	cinfo.image_height = height;
	cinfo.input_components = 3;
	cinfo.in_color_space = JCS_RGB;
	jpeg_set_defaults(&cinfo);

	jpeg_set_quality(&cinfo, 100, true);
	jpeg_start_compress(&cinfo, true);
	int row_stride = width * 3;
	JSAMPROW row_pointer[1];

	while (cinfo.next_scanline < cinfo.image_height) {
		row_pointer[0] = (JSAMPROW) &rgb[cinfo.next_scanline * row_stride];
		jpeg_write_scanlines(&cinfo, row_pointer, 1);
	}

	jpeg_finish_compress(&cinfo);
	jpeg_destroy_compress(&cinfo);



	struct sail_buffer sBuffer = sail_createBuffer(jpeg_buffer_raw, outbuffer_size, true);
	sBuffer.dealoc = true;

	return sBuffer;
}

int clamp(int n, int lower, int upper) {
	return std::max(lower, std::min(n, upper));
}

void YUV422toRGB(const unsigned char* yuv, int pixel_width, int pixel_height, unsigned char* rgb) {
	int i ,j;
	for (i = 0, j = 0; j < 2 * pixel_width * pixel_height ; i+=6, j+=4) {
		unsigned char cb = yuv[j];
		unsigned char y0 = yuv[j+1];
		unsigned char cr = yuv[j+2];
		unsigned char y1 = yuv[j+3];

		rgb[i]   = clamp(round(1.164 * (y0 - 16) + 1.793 * (cr - 128)), 0, 255);
		rgb[i+1] = clamp(round(1.164 * (y0 - 16) - 0.534 * (cr - 128) - 0.213 * (cb-128)), 0 , 255);
		rgb[i+2] = clamp(round(1.164 * (y0 - 16) + 2.115 * (cb - 128)), 0, 255);
		rgb[i+3] = clamp(round(1.164 * (y1 - 16) + 1.793 * (cr - 128)), 0, 255);
		rgb[i+4] = clamp(round(1.164 * (y1 - 16) - 0.534 * (cr - 128) - 0.213 * (cb-128)), 0 , 255);
		rgb[i+5] = clamp(round(1.164 * (y1 - 16) + 2.115 * (cb - 128)), 0, 255);
	}
}

