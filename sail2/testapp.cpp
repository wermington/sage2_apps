// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014

#include "sail2.h"
#include <stdio.h>

//
// usage:  ./testapp hostname:port    (using the http port number)
//         ./testapp localhost:9292
//         ./testapp traoumad.evl.uic.edu
//


#define WIDTH 1920
#define HEIGTH 1024

struct sail_buffer load_image(const char *name)
{
	struct sail_buffer data;
	size_t size;
	FILE *f = fopen(name, "r");
	
	// Read some uncompressed RGB data
	if (!f) {
		fprintf(stderr, "Cannot read [%s]\n", name);
		data.data = NULL;
		return data;
	}
	fseek(f, 0L, SEEK_END);
	size = ftell(f);
	fseek(f, 0L, SEEK_SET);
	data.data = (unsigned char*)malloc(size + 1);

	data.size = fread( (void *) data.data, 1, size, f);
	data.comp = false;
	data.dealoc = false;
	fclose(f);
	return data;
}




struct sail_buffer random_image()
{
	
        struct sail_buffer data;
	
	// Read some uncompressed RGB data
	data.data = (unsigned char*)malloc(WIDTH * HEIGTH * 3);

        int x = rand();
        if(x % 2 == 0)
                memset(data.data, 0, WIDTH * HEIGTH * 3);
        else 
                memset(data.data, 255, WIDTH * HEIGTH * 3);
	data.comp = true;
	data.dealoc = false;
        data.size = WIDTH * HEIGTH * 3;
	return data;
}


int main(int argc, char **argv)
{
	fprintf(stderr, "Main: %s\n", argv[1]);

	sail *sageInf;

	// Read some uncompressed RGB data
        struct sail_buffer image = load_image("pattern.jpg");
        image.id = 1;
//	struct sail_buffer image = random_image();
        if(image.data == NULL) return 1;

	struct sail_buffer image2 = load_image("pattern1.jpg");
	image2.id = 2;
	//struct sail_buffer image2 = random_image();
        if(image.data == NULL) return 2;



	// Create SAIL2 object

        char *path = (char*) "localhost:9292";
        if(argc == 2)
        {
                path = argv[1];
        }
        printf("Connecting to: %s\n", path);


        sageInf = createSAIL("testapp", 1920, 1080, PIXFMT_888, path, 30);
	fprintf(stderr, "App created\n");

	int  count = 0;
	bool done  = false;

	double t1, t2, fps, interval, rate;
	rate = 5.0;
	interval = 1000000.0 / rate;

	while (! done) {
		t1 = getTime();

		// Flip flop
                count++;
		if (count%2)
			sageInf->swapWithBuffer(image);
		else
			sageInf->swapWithBuffer(image2);

		sageInf->processMessages();

		// Wait a little
                usleep(700);
	}

	deleteSAIL(sageInf);
}

